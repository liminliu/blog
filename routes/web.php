<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

/*
$router->get('/', function () use ($router) {
    return 'Hello World';//$router->app->version();
});
*/

/*
$router->get('foo', function () {
    return 'Hello World';//$router->app->version();
});

$router->post('foo', function () {
    return 'Hello World';//$router->app->version();
});
*/

/*
$router->get('user/{id}', function ($id) {
    dd('user'.$id);
});
*/




$router->get('user/{id}/indexPhone', 'UserController@indexPhone');

$router->get('phone/{id}/indexUser', 'PhoneController@indexUser');


//Student--------------------------------------------------------
$router->get('student/{id}/index', 'StudentController@index');

$router->post('student/insert', 'StudentController@insert');

$router->post('student/{id}/update', 'StudentController@update');

$router->delete('student/{id}/delete', 'StudentController@delete');

//Subject--------------------------------------------------------
$router->get('subject/{id}/index', 'SubjectController@index');

$router->post('subject/insert', 'SubjectController@insert');

$router->post('subject/{id}/update', 'SubjectController@update');

$router->delete('subject/{id}/delete', 'SubjectController@delete');

//Relation--------------------------------------------------------
$router->get('relation/{id}/index', 'RelationController@index');

$router->post('relation/insert', 'RelationController@insert');

$router->post('relation/{id}/update', 'RelationController@update');

$router->delete('relation/{id}/delete', 'RelationController@delete');

//middleware--------------------------------------------------------

$router->get('/', ['middleware' => 'token', function () {
    //
    return 'Hell Worldfasdfd';
}]);







