<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/15
 * Time: 5:19
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Relation;
use App\Student;

class RelationController extends Controller
{
    public function index($id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $relation = Relation::where('studentId', $id)->get();//($id);
            return response()->json($relation);
        }
        catch (\Exception $exception){
            Log::error('Error getting index');
        }
    }

    public function insert(Request $request)
    {
        try{
            $relation = Relation::create($request->all());
            return response()->json($relation);
        }
        catch (\Exception $exception){
            Log::error('Error insert');
        }
    }

    public function update(Request $request,$id)
    {
        try{
            $input = collect($request);
            //$id = $input->get('id');
            $relation = Relation::find($id);
            $relation->studentId = $input->get('studentId');
            $relation->courseId = $input->get('courseId');
            $relation->save();
            return response()->json($relation);
        }
        catch (\Exception $exception){
            Log::error('Error update');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $relation = Relation::where('studentId', $id)->delete();
            return response()->json($relation);
        }
        catch (\Exception $exception){
            Log::error('Error delete');
        }
    }
}