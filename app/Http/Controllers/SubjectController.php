<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/15
 * Time: 5:17
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Subject;

class SubjectController extends Controller
{
    public function index($id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $subject = Subject::find($id);
            return response()->json($subject);
        }
        catch (\Exception $exception){
            Log::error('Error getting index');
        }
    }

    public function insert(Request $request)
    {
        try{
            $subject = Subject::create($request->all());
            return response()->json($subject);
        }
        catch (\Exception $exception){
            Log::error('Error insert');
        }
    }

    public function update(Request $request,$id)
    {
        try{
            $input = collect($request);
            //$id = $input->get('id');
            $subject = Subject::find($id);
            $subject->course = $input->get('course');
            $subject->save();
            return response()->json($subject);
        }
        catch (\Exception $exception){
            Log::error('Error update');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $subject = Subject::find($id);
            $subject->delete();
            return response()->json($subject);
        }
        catch (\Exception $exception){
            Log::error('Error delete');
        }
    }
}