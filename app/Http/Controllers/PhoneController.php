<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/18
 * Time: 23:12
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Phone;

class PhoneController extends Controller
{
    public function indexUser($id)
    {
        $user = Phone::find($id)->user;
        return response()->json($user);
    }
}