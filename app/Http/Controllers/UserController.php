<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/13
 * Time: 19:30
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function index($id)
    {
        $user = User::find($id);
        return response()->json($user);
    }

    public function update(Request $request,$id)
    {
        $user = User::find($id);
        $input = collect($request);
        $user->name = $input->get('name');
        $user->save();
        return response()->json($user);
        //return $input->get('name');
    }

    public function indexPhone($id)
    {
        $phone = User::find($id)->phone;
        return response()->json($phone);
    }

    public function show($id)
    {
        return 'afdfdsf';//User::findOrFail($id);
    }
    //
}