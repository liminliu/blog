<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/3/15
 * Time: 5:18
 */

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Student;
use App\Relation;

class StudentController extends Controller
{
    public function index($id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $student = Student::find($id);
            return response()->json($student);
        }
        catch (\Exception $exception){
            Log::error('Error getting index');
        }
    }

    public function insert(Request $request)
    {
        try{
            $student = Student::create($request->all());
            return response()->json($student);
        }
        catch (\Exception $exception){
            Log::error('Error insert');
        }
    }

    public function update(Request $request,$id)
    {
        try{
            $input = collect($request);
            //$id = $input->get('id');
            $student = Student::find($id);
            $student->name = $input->get('name');
            $student->save();
            return response()->json($student);
        }
        catch (\Exception $exception){
            Log::error('Error update');
        }
    }

    public function delete(Request $request,$id)
    {
        try{
            //$input = collect($request);
            //$id = $input->get('id');
            $student = Student::find($id);
            $student->delete();
            return response()->json($student);
        }
        catch (\Exception $exception){
            Log::error('Error delete');
        }
    }
}