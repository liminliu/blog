<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2018/4/10
 * Time: 10:28
 */

namespace App\Http\Middleware;

use Closure;

class CheckToken
{
    public function handle($request, Closure $next)
    {
       if($request->input('token')!='111') {
           return redirect('user/1/indexPhone');
       }
        return $next($request);
    }
}